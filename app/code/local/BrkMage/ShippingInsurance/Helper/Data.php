<?php

/**
 * ShippingInsurance data helper.
 */
class BrkMage_ShippingInsurance_Helper_Data extends Mage_Core_Helper_Abstract
{
	const CONFIG_SHIPPING_INSURANCE_ENABLED = 'insurance/settings/enabled';
	const CONFIG_SHIPPING_METHODS_RATES     = 'insurance/rates/';

	public function isActive()
	{
		return (bool) Mage::getStoreConfig(self::CONFIG_SHIPPING_INSURANCE_ENABLED);
	}

	public function getInsuranceCarrierValue($carrierCode)
	{
		return Mage::getStoreConfig(
			sprintf('%s_value',self::CONFIG_SHIPPING_METHODS_RATES . $carrierCode)
		);
	}

	public function getInsuranceCarrierType($carrierCode)
	{
		return Mage::getStoreConfig(
			sprintf('%s_type', self::CONFIG_SHIPPING_METHODS_RATES . $carrierCode)
		);
	}

	public function checkInsuranceCarrierActive($carrierCode)
	{
		$path = sprintf(
			'%s_active', self::CONFIG_SHIPPING_METHODS_RATES . $carrierCode
		);
		return (int) self::isActive() ? Mage::getStoreConfig($path) : 0;
	}
}
