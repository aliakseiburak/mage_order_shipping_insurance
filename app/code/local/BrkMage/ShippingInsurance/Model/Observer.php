<?php

class BrkMage_ShippingInsurance_Model_Observer
{
	const DISABLED = 0;
	const ENABLED = 1;

	public function checkoutControllerCreatesShippingInsurance(Varien_Event_Observer $observer) {
		if ($observer->getEvent()->getRequest()->getPost('shippinginsurance') === self::ENABLED) {
			$observer->getEvent()->getQuote()->getShippingAddress()->setInsuranceEnable(self::ENABLED);
		} else {
			$observer->getEvent()->getQuote()->getShippingAddress()->setInsuranceEnable(self::DISABLED);
		}
	}
}
