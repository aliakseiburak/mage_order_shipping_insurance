<?php

/**
 * Class BrkMage_ShippingInsurance_Model_System_CloneCarriers
 *
 * Extracts all available Shipping Methods.
 */
class BrkMage_ShippingInsurance_Model_System_CloneCarriers extends Mage_Core_Model_Config_Data
{
	public function getPrefixes()
	{
		$prefixes = array();
		$carriers = Mage::getSingleton('shipping/config')->getAllCarriers();

		foreach ($carriers as $carrier) {
			$prefixes[] = array(
				'field' => sprintf('field_%s_', $carrier->getCarrierCode()),
				'label' => $carrier->getConfigData('title'),
			);
		}

		return $prefixes;
	}
}
