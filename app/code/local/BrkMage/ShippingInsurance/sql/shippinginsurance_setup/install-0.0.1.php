<?php
/* @var $this Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$tables = [
	'sales/order',
	'sales/quote',
	'sales/quote_address'
];

$columns = [
	[
		'name' => 'insurance_amount',
		'definition' => 'NUMERIC (10, 2) DEFAULT 0',
	],
	[
		'name' => 'base_insurance_amount',
		'definition' => 'NUMERIC (10, 2) DEFAULT 0',
	],
	[
		'name' => 'insurance_enable',
		'definition' => 'INT (1) DEFAULT 0',
	],
];

$connection = $installer->getConnection();

foreach ($tables as $table) {
	foreach ($columns as $column) {
		$connection->addColumn(
			$installer->getTable($table),
			$column['name'],
			$column['definition']
		);
	}
}

$installer->endSetup();
